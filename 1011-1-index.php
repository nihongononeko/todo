<?php

    require_once './1011-0.php';

    if (isset($_SESSION['user'])) {

        header('location: 1011-2-todo.php');

        exit;

    }

    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    $message = 'Для авторизации введите логин и пароль';

    $login = !empty($_POST['login']) ? $_POST['login'] : '';
    $pass = !empty($_POST['pass']) ? $_POST['pass'] : '';

    if ($login !== '' && $pass !== '') {

        $inquiry = "SELECT * FROM user WHERE login = :login";

        $stmt = $pdo->prepare($inquiry);
        $stmt->execute(["login" => $login]);
        $result=$stmt->fetchAll(PDO::FETCH_ASSOC);

        if (isset($result[0])) {

            if ($login == $result[0]['login'] && $pass == $result[0]['password']) {

                $_SESSION['id'] = $result[0]['id'];
                $_SESSION['user'] = $result[0]['login'];

                header('location: 1011-2-todo.php');

                exit;

            } else {

                $message = "Вы ввели неверные данные";

            }
            
        } else {

            $message = "Логина $login не существует";

        }
    }
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <title>start</title>
</head>

<body>

<h2>Авторизация</h2>
<p> <?php echo $message ?></p>
<form action="./1011-1-index.php" method="post">
    <input required type="text" name="login" placeholder="login">
    <input required type="text" name="pass" placeholder="password">
<input type="submit" value="Войти">
</form>

<p>или <a href="./1011-0-reg.php">зарегистрируйтесь</a></p>



</body>
</html>