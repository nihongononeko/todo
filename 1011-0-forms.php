<?php

    require_once './1011-0.php';

    if ($_SESSION['id'] == null ) {

        header('location: 1011-1-index.php');

        exit;

    } else {

        $id = !empty($_SESSION['id']) ? $_SESSION['id'] : '';

        $user = !empty($_SESSION['user']) ? $_SESSION['user'] : '';

    }

    if (isset($_POST['taskName'])) {

        $taskName = !empty($_POST['taskName']) ? $_POST['taskName'] : '';

        $inquiryAdd = "INSERT INTO task (user_id, description, date_added) VALUES ('$id', '$taskName', '$date')";

        $stmt = $pdo->prepare($inquiryAdd);

        $stmt -> execute();

        header('location: 1011-2-todo.php');

        exit;

    }

    if (isset($_GET['deletedTask'])) {

        $idDeletedTask = !empty($_GET['deletedTask']) ? $_GET['deletedTask'] : '';

        $inquiryDell = "DELETE FROM task WHERE id = :dell";

        $stmt = $pdo->prepare($inquiryDell);

        $stmt->execute(["dell" => $idDeletedTask]);

        header('location: 1011-2-todo.php');

        exit;

    }

    if (isset($_GET['doneTask'])) {

        $idDoneTask = !empty($_GET['doneTask']) ? $_GET['doneTask'] : '';

        $inquiryDone = "UPDATE task SET is_done = 1 WHERE id = :done";

        $stmt = $pdo->prepare($inquiryDone);

        $stmt->execute(["done" => $idDoneTask]);

        header('location: 1011-2-todo.php');

        exit;

    }

    if (isset($_GET['remakeTask'])) {

        $idRemakeTask = !empty($_GET['remakeTask']) ? $_GET['remakeTask'] : '';

        $inquiryRemake = "UPDATE task SET is_done = 0 WHERE id = :done";

        $stmt = $pdo->prepare($inquiryRemake);

        $stmt->execute(["done" => $idRemakeTask]);

        header('location: 1011-2-todo.php');

        exit;

    }

    foreach ($_POST as $k => $v) {

        if (strpos($k, 'send')) {

            $idSend = (int)$k;

            $recipient = $v;

            $inquiryDelegate = "UPDATE task SET assigned_user_id = :recipient WHERE id = :idSend";

            $stmtDelegate = $pdo->prepare($inquiryDelegate);

            $stmtDelegate->execute(["recipient" => $recipient, "idSend" => $idSend]);

            header('location: 1011-2-todo.php');

            exit;

        }

    }

