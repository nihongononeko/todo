<?php

    require_once './1011-0.php';

    $date = date("Y-m-d H:i:s");

    

    if ($_SESSION['id'] == null ) {

        header('location: 1011-1-index.php');

        exit;

    } else {

        $id = !empty($_SESSION['id']) ? $_SESSION['id'] : '';

        $user = !empty($_SESSION['user']) ? $_SESSION['user'] : '';

    }

    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    // запрос массива дел

    $inquiryActiveTasks = "SELECT task.id, task.description, user.login AS user_id, task.date_added
    FROM `task` 
    LEFT JOIN user ON user.id = task.user_id
    WHERE task.user_id = :id AND task.is_done = 0 OR task.assigned_user_id = :id AND task.is_done = 0
    ORDER BY task.date_added";

    $stmtActiveTasks = $pdo->prepare($inquiryActiveTasks);

    $stmtActiveTasks -> execute(["id" => $id]);

    $activeTasks = $stmtActiveTasks->fetchAll(PDO::FETCH_ASSOC);


    // запрос массива выполненных дел

    $inquiryDoneTasks = "SELECT task.id, task.description, user.login AS user_id, task.date_added
    FROM `task` 
    LEFT JOIN user ON user.id = task.user_id
    WHERE task.user_id = :id AND task.is_done = 1 OR task.assigned_user_id = :id AND task.is_done = 1
    ORDER BY task.date_added";

    $stmtDoneTasks = $pdo->prepare($inquiryDoneTasks);

    $stmtDoneTasks -> execute(["id" => $id]);

    $doneTasks = $stmtDoneTasks->fetchAll(PDO::FETCH_ASSOC);


    // запрос массива пользователей

    $inquiryUsers = "SELECT id, login FROM user";

    $stmtUsers = $pdo->prepare($inquiryUsers);

    $stmtUsers->execute();

    $users=$stmtUsers->fetchAll(PDO::FETCH_ASSOC); // массив пользователей

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <title>start</title>
</head>

<body>

    <h1><?php echo $user ?>, хорошо, что вы сегодня с нами! </h1>

    <h2>Добавить задание</h2>

    <form action="1011-0-forms.php" method="post">

        <input required type="text" name="taskName">

        <input type="submit" value="Добваить задание">

    </form>

    <h2>Сделать <?php $countActive = count($activeTasks); echo $countActive ?></h2>
     
    <table>
        <tr>
            <td><h3>Задание</h3></td>
            <td><h3>Дата</h3></td>
            <td><h3>Поручил</h3></td>
            <td><h3>Делегировать</h3></td>
            <td></td>
            <td></td>
        </tr>

        <?php foreach ($activeTasks as $k) : ?>

            <tr>

                <td> <?php echo $k['description']; ?> </td>

                <td> <?php echo $k['date_added']; ?> </td>

                <td><!-- логин владельца задания задание -->

                    <?php if ($k['user_id'] !== $user) : ?>
                        <?php echo $k['user_id'] ?>
                    <?php endif ?>
                </td><!-- логин владельца задания задание -->

                <td> <!-- делегирование -->
                    <?php if ($k['user_id'] == $user) : ?>

                        <form action="1011-0-forms.php" method="post">

                        <select name="<?php echo $k['id'] ?>send" id="" size="1">

                            <?php foreach ($users as $arr) : ?>

                                <option value="<?php echo $arr['id'] ?>"><?php echo $arr['login'] ?></option>

                            <?php endforeach ?>
                            
                        </select>
                        
                        <input type="submit" value="Делегировать">

                        </form>

                    <?php endif ?>
                </td><!-- делегирование -->

                <td>
                    <a href="./1011-0-forms.php?doneTask=<?php echo $k['id'] ?>">сделано</a>
                </td>

                <td><!-- Удалить задание -->

                    <?php if ($k['user_id'] == $user) : ?>

                        <a href="./1011-0-forms.php?deletedTask=<?php echo $k['id'] ?>">удалить</a>

                    <?php endif ?>
                </td><!-- Удалить задание -->
                
            </tr>

        <?php endforeach ?>
    </table>

        <h2>Сделано <?php $countDone = count($doneTasks); echo $countDone; ?></h2>

    <table>

        <tr>
            <td><h3>Задание</h3></td>
            <td><h3>Дата</h3></td>
            <td><h3>Поручил</h3></td>
            <td></td>
            <td></td>
        </tr>

        <?php foreach ($doneTasks as $k) : ?>

            <tr>

                <td> <?php echo $k['description']; ?> </td>

                <td> <?php echo $k['date_added']; ?> </td>

                <td><!-- логин владельца задания задание -->

                    <?php if ($k['user_id'] !== $user) : ?>

                        <?php echo $k['user_id'] ?>

                    <?php endif ?>

                </td><!-- логин владельца задания задание -->

                <td>

                    <?php if ($k['user_id'] == $user) : ?>

                        <a href="./1011-0-forms.php?remakeTask=<?php echo $k['id'] ?>">переделать</a>

                    <?php endif ?>
                </td>

                <td>

                    <?php if ($k['user_id'] == $user) : ?>

                        <a href="./1011-0-forms.php?deletedTask=<?php echo $k['id'] ?>">удалить</a>

                    <?php endif ?>
                </td>

            </tr>
        <?php endforeach ?>
    </table>

    <h2>
        <a href="./1011-5-logout.php">выйти</a>
    </h2>

</body>
</html>