<?php

    require_once './1011-0.php';

    if (isset($_SESSION['user'])) {

        header('location: 1011-2-todo.php');

        exit;

    }

    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    $login = !empty($_POST['regLogin']) ? trim($_POST['regLogin'], " \t\n\r\0\x0B") : '';

    $pass = !empty($_POST['regPass']) ? trim($_POST['regPass'], " \t\n\r\0\x0B") : '';

    $message = 'Для регистрации введите логин ипароль.';
        
    if ($login !== "" && $pass !== "") {

        $inquiry = "SELECT login FROM user WHERE login = :login";

        $stmt = $pdo->prepare($inquiry);
        $stmt->execute(["login" => $login]);
        $result=$stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($result == null) {

            $inquiry = "INSERT INTO user (login, password) VALUES ('$login', '$pass')";
            $stmt = $pdo->prepare($inquiry);
            $stmt->execute();

            header('location: 1011-1-index.php');

            exit;
            
        } else {

            $message = 'Пользователь с таким логином существует. Выберете другой логин.';

        }
    }


?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <title>start</title>
</head>
<body>
<h2>Регистрация</h2>
<p> <?php echo $message ?> Или <a href="./1011-1-index.php">войдите</a></p>
<form action="./1011-0-reg.php" method="post">
    <input required type="text" name="regLogin" placeholder="login">
    <input required type="text" name="regPass" placeholder="password">
    <input type="submit" value="зарегистрироваться">
</form>

</body>
</html>